---
layout: event
title: "The Wine Well Microchiller"
date: 2018-10-20 00:00:00 -0500
excerpt: Bangor Wine now features a Wine Well, which chills any bottle you select while you check out!
author: Jimmy Jim Bob
featured_image: /assets/img/hero04.jpg
categories:
  - New at Bangor Wine
---

Bangor Wine now features a Wine Well, which chills any bottle you select while you check out!

The fantastic Wine Well Microchiller takes any bottle you select to ice cold serving temperature in no time flat...while you check out, the bottle will swirl in the chilly whirlpool, and you'll be out the door ready to go! No more hoping the store has what you want cold...this can chill what YOU want!
---
layout: event
title: "Kid Central Festival Downtown!"
date: 2019-05-04 00:00:00 -0500
excerpt: Short description about this 2019 event.
featured_image: /assets/img/hero03.jpg
categories:
  - Bangor Events
---


Sat, May 4, 10 AM – 3 PM

<p>Stop by Bangor Wine and Cheese so the kids can sample a cow, goat and sheep cheese.</p>


<p>BangPop! presents the ninth annual Kid Central Festival in Downtown Bangor on Saturday, May 4th. With the help of local businesses, museums and other arts organizations, families will enjoy a wealth of fun and free activities both indoors and outside. From music to crafts, face painting to the annual superhero costume contest, there are activities for everyone ages 12 and under. Kid Central Festival begins at 10 am and continues until 3 pm. Schedule and information will soon be available at www.KidCentralFest.com.</p>
---
layout: event
title:  "The Return of Bangor Uncorked!"
date: 2018-10-22 00:00:00 -0500
excerpt: Spend an evening surrounded by good friends and great wine as you sample from more than 50 wines.
featured_image: /assets/img/hero01.jpg
categories:
  - Bangor Uncorked
---

We proudly announce we are once again a participating sponsor of Bangor Uncorked!
Saturday, October 22nd 4-8pm at the Cross Center: If you like tasting different wines, chatting with friends & shopping, BANGOR UNCORKED is the event for you!

Spend an evening surrounded by good friends and great wine as you sample from more than 50 wines. Choose from some fantastic local and national wines as well as wines from Italy, France, Spain, Australia and South America. We will be available at the event to take orders for the featured wines!

Meet the wine makers, chat with friends and relax with some live music as you browse for fashion, makeup, home decor and more!

Standard Ticket includes: 3 hours of wine sampling from 5pm-8pm, wine samples, souvenir wine glass & live music.

VIP Ticket includes: 4 hours of wine sampling from 4-8pm, wine samples, souvenir wine glass, light appetizers & live music.

*Participants must be at least 21 year of age and have a valid ID to participate*

More information: Participants must be at least 21 years of age to attend and must have valid ID. All tickets are non-refundable. We recommend arriving 30 minutes prior to the event (3:30pm for VIP Ticket & 4:30pm for Standard Ticket) as ID checks must be performed. In order to be admitted to the event you must have your printed paper ticket in hand or a digital ticket on your mobile device. Designated Driver tickets are available at the door for $10; designated drivers must be 21+ to attend. For further questions, please email kathy.hardy@townsquaremedia.com.
---
layout: event
title:  "Monthly Wine Tasting!"
date: 2020-02-27 17:00:00 -0500
excerpt: Sample some of our favorite wines for free at our montly wine tasting!
featured_image: /assets/img/hero06.jpg
categories:
  - At the Shop
---

Thursday, February 27th from 5-7pm join us for our monthly wine tasting! Sample some of our favorite wines for free, must be 21+ to taste.


---
layout: event
title:  "Galentine's/Valentine's Wine tasting!"
date: 2020-02-13 17:00:00 -0500
excerpt: Sample wine while you try free samples from Color Street nails and Perfectly Posh bath products.
featured_image: /assets/img/hero05.jpg
categories:
  - At the Shop
---

Thursday, February 13th from 5-7pm join us for a special Galentine's/Valentine's Wine tasting! Sample wine while you try free samples from Color Street nails and Perfectly Posh bath products.

A great night to spoil yourself! This tasting is free, must be 21+ to taste.

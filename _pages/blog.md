---
layout: page
title: Blog
subtitle: The dog ate our articles
permalink: /blog/
active: blog
slug: blog
---

<h2 class="page-title">{{ page.subtitle | escape }}</h2>

They were here a second ago, I swear.
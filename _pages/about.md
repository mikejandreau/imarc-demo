---
layout: page
title: About
subtitle: Who are these people?
permalink: /about/
active: about
slug: about
---

<h2 class="page-title">{{ page.subtitle | escape }}</h2>

Something about these folks.
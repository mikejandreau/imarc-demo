---
layout: contact
title: Thank You
subtitle: Questions and comments welcome!
permalink: /thanks/
active: thanks
slug: thanks
---

<div class="col-lg-8">
	<h2 class="page-title">{{ page.subtitle | escape }}</h2>
	<p>Thank you for your interest in {{ site.title | escape }}. To contact us you can use the form below or give us a call.</p>
	<p>We can’t wait to hear from you!</p>
	<div class="alert alert-success mb-4" role="alert">
		Message sent successfully, we'll be in touch shortly!
	</div>
</div>

<div class="col-lg-4">
  <div class="sidebar">
    {%- include contact-info.html -%}
  </div>
</div>
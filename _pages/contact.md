---
layout: contact
title: Contact
subtitle: Questions and comments welcome!
permalink: /contact/
active: contact
slug: contact
---

<div class="col-lg-8">
	<h2 class="page-title">{{ page.subtitle | escape }}</h2>
	<p>Thank you for your interest in {{ site.title | escape }}. To contact us you can use the form below or give us a call.</p>
	<p>We can’t wait to hear from you!</p>
	{%- include contact-form.html -%}
</div>

<div class="col-lg-4">
  <div class="sidebar">
    {%- include contact-info.html -%}
  </div>
</div>
